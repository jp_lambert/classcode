<?php


namespace Classcode\Interfaces;


interface IFictionalCrud
{
    public function create(  );
    public static function read(  );
    public function update(  );
    public function delete(  );
}
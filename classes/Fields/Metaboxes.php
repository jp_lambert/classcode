<?php


namespace Classcode\Fields;


class Metaboxes
{
    public function __construct()
    {
        add_action( 'add_meta_boxes', [ $this, "add_fictionnal_textbox" ] );
        add_action( 'save_post_fictional', [ $this, "save_fictional_data" ] );
        add_action( 'save_post_fictional', [ $this, "save_fictional_data_to_db" ] );
    }

    public function add_fictionnal_textbox()
    {
        add_meta_box(
            'fictional_box_id', // Unique ID
            __( 'Description fictional', 'fictional' ),  // Titre
            [ $this, 'fictional_metabox_html' ],  // Le callback
            'fictional'                   // Post type
        );
    }

    public function fictional_metabox_html( $post )
    {
        $value = get_post_meta( $post->ID, '_fictional_field', true );
        ?>
        <label for="fictional_field">Description de la fiction</label>
        <textarea name="fictional_field" id="fictional_field" class="widefat" cols="50" rows="5"><?php echo $value; ?></textarea>
        <?php
    }

    public function save_fictional_data( $post_id )
    {
        if( array_key_exists( 'fictional_field', $_POST ) ) {
            update_post_meta(
                $post_id,
                '_fictional_field',
                wp_filter_nohtml_kses( $_POST[ 'fictional_field' ] )
            );
        }
    }
}
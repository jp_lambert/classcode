<?php
namespace Classcode\Database;

use Classcode\Database\DataStore;

class Datatable
{
    /**
     * Initialization constructor.
     */
    private $current_db_version;
    public function __construct()
    {
        $this->set_current_db_version();
        if ( get_site_option( 'fictionnal_db_version' ) != $this->current_db_version ) {
            $this->database_init();
        }
    }

    public function database_init()
    {
        if (empty($this->current_db_version)) {
            global $wpdb;
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            $sql = "CREATE TABLE IF NOT EXISTS `" . $wpdb->prefix . "table_fictive`(
                ID bigint(20) NOT NULL auto_increment,
                post_id varchar(255) default NULL,
                fictionnal_text longtext,
                PRIMARY KEY  (`ID`)
            );";
            dbDelta( $sql );
            update_option( "fictionnal_db_version", $this->current_db_version );
        }
    }

    public function set_current_db_version(){
        $this->current_db_version = get_option('fictionnal_db_version', 0);
    }
}
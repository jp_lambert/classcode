<?php


namespace Classcode\CustomPost;


class FictionnalPostType
{
    public function __construct(  )
    {
        $this->create_fictional_post_type();
    }

    public function create_fictional_post_type(  )
    {
        $labels = array(
            'name'                => _x( 'Fictions', 'Post Type General Name', 'fictionnal'),
            'singular_name'       => _x( 'Fiction', 'Post Type Singular Name', 'fictionnal'),
            'menu_name'           => __( 'Fiction', 'fictionnal'),
            'all_items'           => __( 'All Fiction', 'fictionnal'),
            'view_item'           => __( 'View all fiction', 'fictionnal'),
            'add_new_item'        => __( 'Add new fiction', 'fictionnal'),
            'add_new'             => __( 'Add', 'fictionnal'),
            'edit_item'           => __( 'Edit', 'fictionnal'),
            'update_item'         => __( 'Modify', 'fictionnal'),
            'search_items'        => __( 'Search', 'fictionnal'),
            'not_found'           => __( 'Not found', 'fictionnal'),
            'not_found_in_trash'  => __( 'Not found in the trash', 'fictionnal'),
        );

        $args = array(
            'label'               => __( 'Fiction', 'fictionnal'),
            'description'         => __( 'Everything is fiction', 'fictionnal'),
            'labels'              => $labels,
            'supports'            => array( 'title' ),
            'show_in_rest'        => true,
            'hierarchical'        => false,
            'public'              => true,
            'has_archive'         => true,
            'rewrite'			  => array( 'slug' => 'fictionnal-post'),

        );

        register_post_type( 'fictional', $args );
    }
}
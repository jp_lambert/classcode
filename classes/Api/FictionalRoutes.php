<?php
namespace Classcode\Api;

use Classcode\Api\FictionalCallbacks;
use Classcode\Interfaces\IFictionalCrud;

class FictionalRoutes implements IFictionalCrud
{
    private $base;
    private $namespace;
    private $version;
    /**
     * FictionalRoutes constructor.
     */
    public function __construct($base,$version, $namespace) {
        $this->base = $base;
        $this->version = $version;
        $this->namespace = $namespace;
        add_action("rest_api_init", [$this,"add_fictional_rest_route"]);
    }

    public function add_fictional_rest_route(  )
    {
        register_rest_route( $this->base."/". $this->version,  $this->namespace, array(
            'methods' => \WP_REST_Server::READABLE,
            'callback' => [$this,"read"],
        ) );
        register_rest_route( 'classcodeapi/v1',  '/fictionnal/', array(
            'methods' => \WP_REST_Server::CREATABLE,
            'callback' => [$this,"create"],
        ) );
    }

    public static function read()
    {
        return new \WP_REST_Response( "data get", 200 );
    }

    public function create()
    {
        return new \WP_REST_Response( "data registered", 200 );
    }

    public function update()
    {
        return new \WP_REST_Response( "data modified", 200 );
    }

    public function delete()
    {
        return new \WP_REST_Response( "data deleted", 200 );
    }
}
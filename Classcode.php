<?php
/**
 * Plugin name: Classcode
 * Plugin URI: https:/fakewebsite.com/classcode
 * Description: Exemple de structure pour un plugin
 * Version: 0.1
 * Author: Jipi Lambert
 * Author URI: takodevweb.com
 */

require __DIR__ . '/vendor/autoload.php';

use Classcode\Database\DataStore;
use Classcode\Database\Datatable;
use Classcode\CustomPost\FictionnalPostType;
use Classcode\Api\FictionalRoutes;
use Classcode\Fields\Metaboxes;

class Classcode
{
    /**
     * Classcode constructor.
     * @usage Add all needed classes initialization.
     */
    public function __construct()
    {
        add_action( 'init', [ $this, 'init' ] );
        register_activation_hook( __FILE__, [ $this, 'init_data' ] );
    }

    public function init()
    {
        new FictionnalPostType();
        new Metaboxes();
        new DataStore();
        new FictionalRoutes( 'classcodeapi/v1', 'v1', 'fictionnal' );
    }

    public function init_data()
    {
        new Datatable();
    }
}

new Classcode();